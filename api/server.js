require('dotenv').config({
  silent: true,
})
const express = require('express')
/**
 * Express app
 */
const app = express()

app.use('/api', require('./index'))

app.listen(process.env.DEV_API_PORT || 3005, () =>
  console.log('OK AT ', process.env.DEV_API_PORT || 3005)
)
