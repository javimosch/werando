const db = require('mongoose')
const jwt = require('jsonwebtoken')

require('dotenv').config({
  silent: true,
})
const express = require('express')

/**
 * Database configuration/init
 */
require('./mongo').MongoWrapper.init()

/**
 * Services
 */
require('./services/mailing')
require('./services/schedules')

/**
 * API Router
 */
const app = express()

const morgan = require('morgan')
app.use(
  morgan(function (tokens, req, res) {
    return [
      tokens.method(req, res),
      tokens.url(req, res),
      tokens.status(req, res),
      tokens.res(req, res, 'content-length'),
      '-',
      tokens['response-time'](req, res),
      'ms',
    ].join(' ')
  })
)

app.use(
  '/uploads',
  express.static(require('path').join(process.cwd(), 'uploads'))
)

app.get('/', (req, res) => {
  res.redirect('/api/docs')
})
app.use('/docs', express.static(require('path').join(__dirname, '/docs')))
app.use(express.json())
app.get('/alive', (req, res) => {
  res.json({ result: true })
})

const session = require('express-session')
const MongoDBStore = require('connect-mongodb-session')(session)
const store = new MongoDBStore({
  uri: process.env.MONGO_URI,
  collection: 'sessions',
  connectionOptions: {
    dbName: process.env.MONGO_DB,
    useNewUrlParser: true,
    useUnifiedTopology: true,
  },
})

// Catch errors
store.on('error', function (error) {
  console.log(error)
})

app.use(
  session({
    secret: process.env.JWT_SECRET || 'secret',
    resave: true,
    saveUninitialized: false,
    store,
    cookie: { secure: true, maxAge: 1000 * 60 * 60 * 24 /** 24 hours */ },
  })
)

app.authenticate = () => {
  function jwtverify(token) {
    return new Promise((resolve, reject) => {
      jwt.verify(
        token,
        process.env.JWT_SECRET || 'secret',
        function (err, decoded) {
          ;(err && reject(err)) || resolve(decoded)
        }
      )
    })
  }
  return async function (req, res, next) {
    let token = ''
    if (
      req.headers.authorization &&
      req.headers.authorization.split(' ')[0] === 'Bearer'
    ) {
      token = req.headers.authorization.split(' ')[1]
      try {
        const decoded = (token && (await jwtverify(token))) || null
        if (decoded && decoded.data && decoded.data.userId) {
          req.user =
            (await db.model('user').findById(decoded.data.userId)) || null
        }
      } catch (err) {
        if (!err.stack.includes('TokenExpiredError')) {
          throw err
        }
      }
    }
    if (!req.user) {
      return res.status(401).send()
    }
    next()
  }
}

app.use('/events', require('./event')(app))
app.use('/users', require('./user')(app))
app.use('/models', require('./model')(app))
require('./mongoose-crud')(app)

module.exports = app
