const mongoose = require('mongoose')
const defaultDomain = 'misitioba.com'
const defaultAppName = 'W&M'
const defaultHost = 'werando.misitioba.com'
const host = process.env.PUBLIC_HOST || defaultHost
const appName = process.env.APP_NAME || defaultAppName

mongoose.srv = mongoose.srv || {}
mongoose.srv.mailing = {}
mongoose.srv.mailing.sendAcountCreated = function sendAcountCreated(user) {
  sendHTMLEmailWithMailgun({
    to: [user.email],
    subject: `Your ${appName} account has been created`,
    text: `Your ${appName} account has been created. Welcome!`,
    html: `<b>Welcome!</b>
<br/>
Hey ${user.username || user.email},
<br/>
Welcome to ${appName}! We are thrilled you join us!
<br/>
What happens next?
Keep an eye on the next hikes <a href="${host}/events" target="_blank">here</a>, sign up and meet other people like you while discovering Montpellier and its surroundings!
<br/>
We would love to hear what you think of W&M and if there is anything we can improve to ensure you get the best experience.
<br/>
Hope to see you soon and take care!
    `,
  })
}
mongoose.srv.mailing.sendPasswordReset = function sendPasswordReset(
  email,
  resetKey
) {
  sendHTMLEmailWithMailgun({
    to: [email],
    subject: `Password reset request`,
    text: `Hey! Lost your password? Follow this URL: ${host}/api/users/password_reset/hook?key=${resetKey}`,
    html: `
    Hey! Lost your password? <br/>
    Click on the link below to reset it :
    <br/><br/>
    <a href="${host}/api/users/password_reset/hook?key=${resetKey}" target="_blank">LINK</a>
    `,
  })
}

function sendHTMLEmailWithMailgun(options = {}) {
  return new Promise((resolve, reject) => {
    const domain = process.env.MAILGUN_DOMAIN || defaultDomain
    const mailgun = require('mailgun-js')({
      apiKey: process.env.MAILGUN_API_KEY,
      domain,
    })
    const MailComposer = require('nodemailer/lib/mail-composer')

    const mailOptions = {
      from: `admin@${domain}`,
      to: options.to,
      subject: options.subject || '(No subject)',
      text: options.text || '(No text) :(',
      html: options.html || '(No <strong>html</strong>) :(',
    }

    const mail = new MailComposer(mailOptions)

    mail.compile().build((err, message) => {
      if (err) {
        console.error('sendHTMLEmailWithMailgun', err)
        return reject(err)
      }

      const dataToSend = {
        to: options.to,
        message: message.toString('ascii'),
      }

      mailgun.messages().sendMime(dataToSend, (sendError, body) => {
        if (sendError) {
          console.error('sendHTMLEmailWithMailgun', sendError)
          reject(sendError)
        } else {
          console.log(
            'MAILING::sendHTMLEmailWithMailgun',
            options.to,
            mailOptions,
            'OK'
          )
          resolve(body)
        }
      })
    })
  })
}
