const db = require('mongoose')
const moment = require('moment')
module.exports = async function closeOldEvents() {
  const openEvents = await db
    .model('event')
    .find({
      $or: [
        { status: 'open' },
        {
          status: null,
        },
      ],
      date: {
        $lt: moment()._d,
      },
    })
    .select('_id')
    .exec()
  if (openEvents.length > 0) {
    await db.model('event').updateMany(
      {
        _id: {
          $in: openEvents.map((e) => e._id),
        },
      },
      {
        $set: {
          status: 'closed',
        },
      }
    )
  }
}
