export default function ({ app, store }) {
  app.router.beforeEach(async (to, from, next) => {
    if (to.meta && to.meta.requiresAdmin) {
      if (process.server) {
        return next()
      }
      if (!store.getters['auth/isLogged']) {
        await store.dispatch('auth/loadCache')
      }

      if (
        !(store.getters['auth/isLogged'] && store.getters['auth/isAppAdmin'])
      ) {
        return next(false)
      }
    }
    next()
  })
}
