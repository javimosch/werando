import Vue from 'vue'
import Dropzone from 'dropzone'
import 'dropzone/dist/dropzone.css'
Dropzone.autoDiscover = false

Vue.use({
  install() {
    Vue.$Dropzone = Vue.prototype.$Dropzone = Dropzone
  },
})
