export default (ctx, inject) => {
  // Inject $hello(msg) in Vue, context and store.
  inject('host', () => {
    const isHttps = ctx.req.headers.host.includes('https')
    const host =
      process.env.NUXT_ENV_API_HOST ||
      `http${isHttps ? 's' : ''}://` + ctx.req.headers.host
    return host
  })
}
