require('dotenv').config({
  silent: true,
})

let proxy = {}
let serverMiddleware = []

const isProduction = process.env.NODE_ENV === 'production'

if (isProduction) {
  serverMiddleware = [{ path: '/api', handler: '~/api/index.js' }]
} else {
  proxy = {
    '/api': `http://localhost:${process.env.DEV_API_PORT || 3005}`,
  }
}

console.log('MODE', isProduction ? 'Prod' : 'Dev')
process.VUE_APP_NODE_ENV = isProduction ? 'production' : 'development'

export default {
  dev: !isProduction,
  // Global page headers: https://go.nuxtjs.dev/config-head
  head: {
    title: 'Walk&Meed',
    meta: [
      {
        charset: 'utf-8',
      },
      {
        name: 'viewport',
        content: 'width=device-width, initial-scale=1',
      },
      {
        hid: 'description',
        name: 'description',
        content: '',
      },
    ],
    link: [
      {
        rel: 'icon',
        type: 'image/x-icon',
        href: '/favicon.ico',
      },
      {
        rel: 'stylesheet',
        href:
          'https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.14.0/css/all.min.css',
      },
    ],
  },

  // Global CSS: https://go.nuxtjs.dev/config-css
  css: [],

  // Plugins to run before rendering page: https://go.nuxtjs.dev/config-plugins
  plugins: [
    {
      src: '@/plugins/host.server.js',
    },
    {
      src: '@/plugins/localforage',
      mode: 'client',
    },
    {
      src: '@/plugins/dropzone.client.js',
      mode: 'client',
    },
    {
      src: '@/plugins/axios',
    },
    {
      src: '@/plugins/auth-middleware',
    },
  ],

  // Auto import components: https://go.nuxtjs.dev/config-components
  components: true,

  // Modules for dev and build (recommended): https://go.nuxtjs.dev/config-modules
  buildModules: [
    // https://go.nuxtjs.dev/eslint
    '@nuxtjs/eslint-module',
  ],

  serverMiddleware,

  // Modules: https://go.nuxtjs.dev/config-modules
  modules: [
    // https://go.nuxtjs.dev/bootstrap
    // https://go.nuxtjs.dev/axios
    '@nuxtjs/axios',
    // https://go.nuxtjs.dev/pwa
    '@nuxtjs/pwa',
    '@nuxtjs/style-resources',
    '@nuxtjs/proxy',
    'nuxt-route-meta',
    'nuxt-i18n',
  ],

  i18n: {
    locales: [
      {
        code: 'fr',
        file: 'fr-FR.js',
      },
      {
        code: 'es',
        file: 'es-ES.js',
      },
      {
        code: 'en',
        file: 'en-US.js',
      },
    ],
    defaultLocale: 'en',
    strategy: 'prefix_except_default',
    lazy: true,
    langDir: 'lang/',
    vueI18n: {
      fallbackLocale: 'en',
    },
  },

  proxy,

  // Axios module configuration: https://go.nuxtjs.dev/config-axios
  axios: {
    baseURL: '/api',
    credentials: true,
  },

  // PWA module configuration: https://go.nuxtjs.dev/pwa
  pwa: {
    manifest: {
      lang: 'en',
    },
  },

  vue: {
    config: {
      productionTip: false,
      devtools: process.env.NODE_ENV !== 'production',
    },
  },

  // Build Configuration: https://go.nuxtjs.dev/config-build
  build: {
    extend(config, ctx) {
      if (ctx.isDev) {
        /*
        config.module.rules.push({
          enforce: 'pre',
          test: /\.(js|vue)$/,
          loader: 'eslint-loader',
          exclude: /(node_modules)/,
          options: {
            fix: true,
          },
        }) */

        if (!isProduction) {
          config.module.rules.push({
            test: /\.vue$/,
            use: [
              {
                loader: 'vue-pretty-logger',
                options: {},
              },
            ],
          })
        }
      }
    },
  },
}
