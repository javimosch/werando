import axios from 'axios'

export default {
  computed: {
    loggedUser() {
      return this.$store.getters['auth/loggedUser']._id
        ? this.$store.getters['auth/loggedUser']
        : null
    },
    loggedUserIsSubscribed() {
      const id = this.$store.getters['auth/loggedUser']._id
      return !!((this.item || this.event).users || []).find(
        (eu) => (eu.user._id || eu.user).toString() === id
      )
    },
  },
  methods: {
    async unsuscribeFromEvent(event) {
      await axios.post(`/api/events/unjoin_event`, {
        event,
        user: this.$store.getters['auth/loggedUser'],
      })
      this.update()
    },
    async joinEvent(e, event) {
      e.stopPropagation()
      try {
        await axios.post(`/api/events/join_event`, {
          event,
          user: this.$store.getters['auth/loggedUser'],
        })

        this.$router.push(
          this.localePath({
            name: 'events-slug',
            params: {
              slug: event.computedSlug,
            },
          })
        )
        this.update && this.update()
        if (this.event && this.event.computedSlug) {
          this.$router.push(
            this.localePath({
              name: 'events-slug',
              params: { slug: this.event.computedSlug },
            })
          )
        }
      } catch (err) {
        if (err.response && err.response.status === 401) {
          this.$store.dispatch('auth/logout', {
            routeToLogin: false,
          })
        }
      }
    },
  },
}
