import { getTranslations } from './index'
export default async (context) => {
  return (await getTranslations(context, 'en')).docs.reduce((acum, t) => {
    acum[t.path] = t.value
    return acum
  }, {})
}
