const fetch = require('node-fetch')

export async function getTranslations(context, language = 'en') {
  let apiURL
  if (process.client) {
    apiURL = window.location.origin
  } else {
    apiURL = (context.$host && context.$host()) || context.req.headers.host
  }
  apiURL = `http${apiURL.includes('localhost') ? '' : 's'}://${apiURL
    .split('http://')
    .join('https://')
    .split('https://')
    .join('')}`

  return await fetch(
    `${apiURL}/api/translations?q.locale=${language}&limit=99999`
  ).then((res) => res.json())
}
