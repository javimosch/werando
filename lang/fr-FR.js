import { getTranslations } from './index'
export default async (context) => {
  return (await getTranslations(context, 'fr')).docs.reduce((acum, t) => {
    acum[t.path] = t.value
    return acum
  }, {})
}
